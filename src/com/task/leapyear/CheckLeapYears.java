package com.task.leapyear;

public class CheckLeapYears {

	public static boolean checkIfLeapYear(Integer i) {
		if((i%400==0) || ((i%100!=0) && (i%4==0))) {
			return true;
		}
		return false;
	}
	
}
