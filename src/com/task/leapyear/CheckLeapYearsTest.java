package com.task.leapyear;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CheckLeapYearsTest {

	@Test
	void testCheckIfLeapYear_1() {
		assertEquals(true, CheckLeapYears.checkIfLeapYear(2000));
	}
	
	@Test
	void testCheckIfLeapYear_2() {
		assertEquals(false,CheckLeapYears.checkIfLeapYear(1700));
	}
	
	@Test
	void testCheckIfLeapYear_3() {
		assertEquals(true,CheckLeapYears.checkIfLeapYear(2008));
	}

}
